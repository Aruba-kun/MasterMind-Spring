CREATE TABLE game (
    id INT PRIMARY KEY,
    sequence VARCHAR(10),
    player1 VARCHAR(20),
    player2 VARCHAR(20)
);