package fr.alban.repositories;

import fr.alban.models.Turn;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Aruba-kun on 07/02/2017.
 */
public interface TurnRepository extends CrudRepository<Turn, String> {
    @Query("SELECT t FROM Turn t WHERE t.game = :game ORDER BY t.uuid DESC")
    List<Turn> getAllByGame(@Param("game") final Integer game);
}
