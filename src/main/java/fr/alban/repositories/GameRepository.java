package fr.alban.repositories;

import fr.alban.models.Game;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Aruba-kun on 06/02/2017.
 */
public interface GameRepository extends CrudRepository<Game, Integer> {

}
