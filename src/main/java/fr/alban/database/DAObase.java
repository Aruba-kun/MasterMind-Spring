package fr.alban.database;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by Aruba-kun on 06/02/2017.
 */

@Configuration
public class DAObase {
    @Bean
    public DataSource dataSource() throws FileNotFoundException {
        File db = ResourceUtils.getFile("database.db");

        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.sqlite.JDBC");
        dataSourceBuilder.url("jdbc:sqlite:"+db.getPath());

        return dataSourceBuilder.build();
    }
}
