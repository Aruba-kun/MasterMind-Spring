package fr.alban.models;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Aruba-kun on 06/02/2017.
 */

@Entity
@Table(name = "game")
public class Game implements Serializable {
    @Id
    @Column(name = "id")
    public Integer id;

    @Column(name = "sequence")
    private String sequence;

    @Column(name = "player1")
    public String player1;

    @Column(name = "player2")
    public String player2;

    @Column(name = "started")
    public boolean started;

    @Column(name = "finished")
    public boolean finished;

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
}
