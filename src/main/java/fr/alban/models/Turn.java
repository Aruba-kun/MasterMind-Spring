package fr.alban.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Aruba-kun on 07/02/2017.
 */

@Entity
@Table(name = "Turn")
public class Turn implements Serializable {
    @Id
    @Column(name = "uuid")
    public String uuid;

    @Column(name = "game")
    public Integer game;

    @Column(name = "player")
    public String player;

    @Column(name = "proposition")
    public String proposition;

    @Column(name = "hints")
    public String hints;
}
