package fr.alban.controllers;

import fr.alban.controllers.responses.GameNotFoundException;
import fr.alban.controllers.responses.NotAllowedPlayerException;
import fr.alban.models.Game;
import fr.alban.models.Player;
import fr.alban.models.Result;
import fr.alban.models.Turn;
import fr.alban.repositories.GameRepository;
import fr.alban.repositories.TurnRepository;
import fr.alban.utils.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import java.util.Random;

/**
 * Created by Aruba-kun on 06/02/2017.
 */

@RestController
@RequestMapping("/game")
public class GameController {

    @Autowired
    private TurnController turnController;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private TurnRepository turnRepository;


    @Produces(value = "application/json")
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public Iterable<Game> getAll() {
        Iterable<Game> games = gameRepository.findAll();

        return games;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Game getById(@PathVariable(value = "id") Integer id) {

        Game game = gameRepository.findOne(id);

        return game;
    }

    @RequestMapping(value = "/choice/{gameId}", method = RequestMethod.POST, produces = "application/json")
    public Result choiceResponse(
            @PathVariable(value = "gameId") Integer gameId,
            @RequestParam(name = "key") String key,
            @RequestParam(name = "sequence") String sequence
    ) {
        Game game = gameRepository.findOne(gameId);
        Player player = checkGame(game, key);

        Turn last = turnController.getLastForGame(gameId);
        if(!last.player.equals(key) || last.proposition != null) {
            throw new NotAllowedPlayerException();
        }

        String hints;
        Result result = new Result();
        result.game = gameId;

        if(game.getSequence().equals(sequence)) {
            result.winner = player.number;
            game.finished = true;
            gameRepository.save(game);
        } else {
            result.hints = getHints(sequence, game.getSequence());
            hints = result.hints;
            turnController.goToNextTurn(game, player, sequence, hints);
        }

        return result;
    }

    @RequestMapping(value = "/attack/{gameId}", method = RequestMethod.POST, produces = "application/json")
    public Game choiceAttack(
            @PathVariable(value = "gameId") Integer gameId,
            @RequestParam(name = "key") String key
    ) {
        Game game = gameRepository.findOne(gameId);
        Player player = checkGame(game, key);

        return game;
    }

    public Game createNewGame(Integer gameId) {
        RandomString randomString = new RandomString(20);
        Game game = new Game();
        game.id = gameId;
        game.setSequence(generateSequence());

        if(game.player1 == null) {
            game.player1 = randomString.nextString();
            gameRepository.save(game);
        }
        game = this.gameRepository.save(game);

        Turn turn = new Turn();
        turn.uuid = game.id+"-0";
        turn.game = game.id;
        turn.player = game.player1;

        this.turnRepository.save(turn);
        return game;
    }

    private Player checkGame(Game game, String key) {
        if(game == null) {
            System.out.println("Game not found");
            throw new GameNotFoundException();
        }

        if(game.player1 == null || game.player2 == null){
            System.out.println("Game not available");
            throw new GameNotFoundException();
        }

        if(!(key.equals(game.player1) || key.equals(game.player2))){
            System.out.println("Key not valid");
            throw new GameNotFoundException();
        }

        Player player = new Player();
        player.key = key;
        if(key.equals(game.player1)) {
            player.number = 1;
        } else {
            player.number = 2;
        }

        return player;
    }

    private String getHints(String seq, String answer) {
        StringBuilder hints = new StringBuilder();

        for (int i = 0; i<5; i++) {
            Character character = seq.charAt(i);

            if(character.equals(answer.charAt(i))) {
                hints.insert(i, '1');
            } else {
                hints.insert(i, '0');
            }
        }

        return hints.toString();
    }

    private String generateSequence() {
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();

        for(int  i = 0; i<5; i++) {
            stringBuilder.append(random.nextInt(5));
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(GameController.class, args);
    }
}
