package fr.alban.controllers;

import fr.alban.models.Game;
import fr.alban.models.Player;
import fr.alban.models.Turn;
import fr.alban.repositories.TurnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Aruba-kun on 07/02/2017.
 */

@RestController
@RequestMapping("/turn")
public class TurnController {
    @Autowired
    private TurnRepository turnRepository;

    @RequestMapping(value = "/{game}", method = RequestMethod.GET, produces = "application/json")
    public List<Turn> getAllForGame(@PathVariable(name = "game") final Integer game) {

        List<Turn> list = turnRepository.getAllByGame(game);
        return list;
    }

    public Turn getLastForGame(final Integer game) {

        List<Turn> list = turnRepository.getAllByGame(game);
        return list.get(0);
    }

    public Turn goToNextTurn(final Game game, final Player player, final String proposition, String hints) {
        Turn last = getLastForGame(game.id);
        last.proposition = proposition;
        last.hints = hints;
        this.turnRepository.save(last);

        Turn next = new Turn();
        next.uuid = getNextUuid(game,last);
        next.player = player.number == 1 ? game.player2 : game.player1;
        next.game = game.id;

        next = this.turnRepository.save(next);
        return next;
    }

    private String getNextUuid(final Game game, final Turn previous) {
        String prevUuid = previous.uuid;
        Integer number = Integer.parseInt(prevUuid.substring(prevUuid.indexOf("-")+1));

        return game.id+"-"+(number+1);
    }
}
