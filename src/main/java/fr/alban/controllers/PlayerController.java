package fr.alban.controllers;

import fr.alban.controllers.responses.NotAllowedPlayerException;
import fr.alban.models.Game;
import fr.alban.models.Player;
import fr.alban.repositories.GameRepository;
import fr.alban.utils.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Aruba-kun on 06/02/2017.
 */

@RestController
@RequestMapping("/player")
public class PlayerController {
    @Autowired
    private GameController gameController;

    @Autowired
    private GameRepository gameRepository;

    @RequestMapping(value = "/{gameId}", method = RequestMethod.GET, produces = "application/json")
    public Player getAvailableKey(@PathVariable Integer gameId) {
        RandomString randomString = new RandomString(20);
        Game game = gameController.getById(gameId);
        if(game == null) {
            game = gameController.createNewGame(gameId);
        }
        Player player = new Player();

        if(!game.started && game.player2 == null ) {
            player.key = game.player1;
            player.number = 1;
            game.player2 = randomString.nextString();
        } else if(game.player2 != null){
            player.key = game.player2;
            player.number = 2;
            game.started = true;

        } else {
            throw new NotAllowedPlayerException();
        }

        gameRepository.save(game);
        return player;
    }
}
