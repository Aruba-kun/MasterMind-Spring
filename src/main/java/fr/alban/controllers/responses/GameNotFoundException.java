package fr.alban.controllers.responses;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Aruba-kun on 06/02/2017.
 */

@ResponseStatus(value = HttpStatus.CONFLICT)
public class GameNotFoundException extends RuntimeException {
}
